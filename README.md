# Extension-Debug-Icon
Version 0.2

## Installation  
- download the project from https://gitlab.com/Franci5co/debugMode/-/archive/master/debugMode-master.zip
- go to web: `chrome://extensions/`   
- switch on: `Developer Mode`  
- click `LOAD UNPACKED` button   
- search the folder and click `SELECT` button  
- open your app

**Only for developers:**
- go to web: `chrome://extensions/` and copy the `ID` value
- open `script.js` change the variable `let editorExtensionId` to you copied ID (paste it)
- save `script.js`
- go to `chrome://extensions/`, press refresh icon in `Debug Mode` card


## How it works
Open your app in the browser, if debug icon is in red, you´re already in debug mode, if the icon is in gray you are'nt.  
Click the icon if it's in gray to switch to debug mode, the icon will turns red.
  
## On Load
It detects if you're in debug mode
![](https://gitlab.com/Franci5co/debugMode/raw/master/assets/onLoad.gif)


## On Click
It runs aqfDebugMode() function
![](https://gitlab.com/Franci5co/debugMode/raw/master/assets/onClick.gif)