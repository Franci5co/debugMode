(function() {
	'use strict';

	// On load
	chrome.runtime.onMessageExternal.addListener(
		function(request, sender, sendResponse) {
			setTimeout(function(){
				chrome.browserAction.setIcon({
					path:"assets/icon2.png",
					tabId: sender.tab.id
				});
			}, 2000);
		}
	);

	// On icon click
	chrome.browserAction.onClicked.addListener(function(parm) {
		chrome.tabs.executeScript(null, {file: "toggleIcon.js"});
		chrome.tabs.executeScript(null, {file: "content.js"});
	});

	// After icon click
	chrome.runtime.onMessage.addListener(
		function(request, sender, sendResponse) {

			if(request.changeIcon) {
				chrome.browserAction.setIcon({
					path:"assets/icon1.png",
					tabId: sender.tab.id
				});
				setTimeout(function(){
					chrome.browserAction.setIcon({
						path:"assets/icon2.png",
						tabId: sender.tab.id
					});
				}, 2000);
			}
		}
	);

})();