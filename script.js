(function() {
	'use strict';

	let fromOnclick = localStorage.getItem('fromOnclick');
	localStorage.setItem('fromOnclick', '');

	if ( isDebug()) {
		let editorExtensionId = "ipodafafpoohodahjhnjdcdffkkdekfj";
		// myParam (optional parameter)
		chrome.runtime.sendMessage(editorExtensionId, {myParam: true},
			function(response) {
			});
	}

	if (fromOnclick === 'true') {
		window.aqfDebugMode();
	}

	function isDebug() {
		try {
			return (window.configKiosk && window.configKiosk.debug) || // Kiosk Web
				(window.aqfReaderSettings && window.aqfReaderSettings.debug) || // ReaderPdf
				(window.config && window.config.debug) || // Factory && CC && RS (bug)
				(window.aqfAppConfig && window.aqfAppConfig.debug) // CA
		} catch(err) {
			return false;
		}
	}

})();

